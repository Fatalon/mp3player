use embedded_hal::spi;
use sh1106::{mode::GraphicsMode, interface::DisplayInterface, prelude::*, Builder};
pub struct OledHat<DI>
where DI:DisplayInterface,
{
    display:GraphicsMode<DI>
}

#[derive(Debug)]
pub enum OledError{
    DisplayError,
    WidgetNotFound
    
}
// DisplayInterface::Error
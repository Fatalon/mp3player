#![no_std]
#![no_main]

// use avr_hal_generic::port::PinOps;
use arduino_hal::prelude::*;
use arduino_hal::spi::Settings;
use embedded_hal::spi as e_spi;

use avr_hal_generic::spi::{DataOrder,SerialClockRate};

use sh1106::{mode::GraphicsMode, prelude::*, Builder};

use embedded_graphics::{
    image::{Image, ImageRawLE},
    pixelcolor::BinaryColor,
    prelude::*,
};

use panic_halt as _;

const UART_BAUDRATE: u32 = 57600;

#[arduino_hal::entry]
fn main() -> ! {
    let dp = arduino_hal::Peripherals::take().unwrap();
    let pins = arduino_hal::pins!(dp);

    let mut serial = arduino_hal::default_serial!(dp, pins, UART_BAUDRATE);

    ufmt::uwriteln!(&mut serial, "Hello from Arduino!\r").void_unwrap();

    let dc = pins.d8.into_output();
    let _reset = pins.d9.into_output_high();

    let spi_settings = Settings{
        data_order: DataOrder::MostSignificantFirst,
        clock: SerialClockRate::OscfOver128,
        mode: e_spi::MODE_0,
    };
    // Create SPI interface.
    let (spi, cs) = arduino_hal::Spi::new(
        dp.SPI,
        pins.d13.into_output(),
        pins.d11.into_output(),
        pins.d12.into_pull_up_input(),
        pins.d10.into_output(),
        spi_settings
    );

    

    let mut display: GraphicsMode<_> = Builder::new().connect_spi(spi, dc, cs).into();

    display.init().unwrap();
    display.flush().unwrap();

    // let im: ImageRawLE<BinaryColor> = ImageRawLE::new(include_bytes!("rust.raw"), 64);

    // Image::new(&im, Point::new(32, 0))
    //     .draw(&mut display)
    //     .unwrap();
    // display.set_pixel(10, 20, 1);

    // Top side
    display.set_pixel(0, 0, 1);
    display.set_pixel(1, 0, 1);
    display.set_pixel(2, 0, 1);
    display.set_pixel(3, 0, 1);

    // Right side
    display.set_pixel(3, 0, 1);
    display.set_pixel(3, 1, 1);
    display.set_pixel(3, 2, 1);
    display.set_pixel(3, 3, 1);

    // Bottom side
    display.set_pixel(0, 3, 1);
    display.set_pixel(1, 3, 1);
    display.set_pixel(2, 3, 1);
    display.set_pixel(3, 3, 1);

    // Left side
    display.set_pixel(0, 0, 1);
    display.set_pixel(0, 1, 1);
    display.set_pixel(0, 2, 1);
    display.set_pixel(0, 3, 1);


    display.flush().unwrap();

    loop {
        // // Read a byte from the serial connection
        // let b = nb::block!(serial.read()).void_unwrap();

        // // Answer
        // ufmt::uwriteln!(&mut serial, "Got {}!\r", b).void_unwrap();

        // Send a byte
        // nb::block!(spi.send(0b00001111)).void_unwrap();
        // // Because MISO is connected to MOSI, the read data should be the same
        // let data = nb::block!(spi.read()).void_unwrap();

        // ufmt::uwriteln!(&mut serial, "data: {}\r", data).void_unwrap();
        //arduino_hal::delay_ms(1000);
    }
}
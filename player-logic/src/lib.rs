#![no_std]

use player_state_machine::*; 
struct PlayerContent{
    song_id: u8,
    volume:u16,
    played_time_in_s: u16 
}


pub mod player_state_machine;
#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works() {
        let playerState = PlayerStateMachine::new();

        //assert_eq!(playerState.state, Idle);
        let PlayerState = PlayerStateMachine::<Running>::from(playerState);
    }
}

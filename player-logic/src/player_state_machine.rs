/// State machine representing the music player
/// inspired by https://hoverbear.org/blog/rust-state-machine-pattern/
/// currently contains no internal state, only simple transitions
pub struct PlayerStateMachine<S>{
    state: S
}

impl PlayerStateMachine<Idle> {
    /// Init method
    /// ```
    /// // run via
    /// use player_logic::player_state_machine::*;
    /// let playerState = PlayerStateMachine::new();
    /// ```
    /// 
    pub fn new() -> Self {
        PlayerStateMachine { state:Idle }
    }
}


/// allowed internal state for PlayerStateMachine
pub struct Idle;
/// allowed internal states for PlayerStateMachine
pub struct Running;
/// allowed internal states for PlayerStateMachine
pub  struct Paused;

// pref FROM trait, automatically will generate impl for TO trait and is more expressive
impl From<PlayerStateMachine<Idle>> for PlayerStateMachine<Running> {
    fn from(_: PlayerStateMachine<Idle>) -> PlayerStateMachine<Running> {
        PlayerStateMachine { state: Running,  }
    }
}

impl From<PlayerStateMachine<Running>> for PlayerStateMachine<Paused> {
    fn from(_: PlayerStateMachine<Running>) -> PlayerStateMachine<Paused> {
        PlayerStateMachine { state: Paused,  }
    }
}

impl From<PlayerStateMachine<Running>> for PlayerStateMachine<Idle> {
    fn from(_: PlayerStateMachine<Running>) -> PlayerStateMachine<Idle> {
        PlayerStateMachine { state: Idle,  }
    }
}

impl From<PlayerStateMachine<Paused>> for PlayerStateMachine<Running> {
    fn from(_: PlayerStateMachine<Paused>) -> PlayerStateMachine<Running> {
        PlayerStateMachine { state: Running,  }
    }
}

impl From<PlayerStateMachine<Paused>> for PlayerStateMachine<Idle> {
    fn from(_: PlayerStateMachine<Paused>) -> PlayerStateMachine<Idle> {
        PlayerStateMachine { state: Idle,  }
    }
}